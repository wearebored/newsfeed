const express = require("express");
const bodyParser = require("body-parser");
const InsertData = require("./database"); 
const cors = require("cors");
const NewsGet = require("./newsget");

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.post("/register", async (req, res) => {
  const requestData = req.body;
  try {
    const sorgudata = {
      username: requestData.username,
    };
    const sonuc = await InsertData("users", sorgudata, "sorgu");
    if (sonuc.length > 0) {
      res.json({
        message: "User already exists",
        status: "fail",
      });
    } else {
      const sonuc = await InsertData("users", requestData, "ekle");
      sonuc
        ? res.json({
            message: "Your registration is completed",
            status: "success",
          })
        : res.json({
            message: "An error occurred during registration",
            status: "fail",
          });
    }
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred during the database operation" });
  }
});
app.post("/login", async (req, res) => {
  const requestData = req.body;
  try {
    const sorgudata = {
      username: requestData.username,
    };
    const sonuc = await InsertData("users", sorgudata, "sorgu");
    if (sonuc.length > 0) {
      if (sonuc[0].password === requestData.password) {
        res.json({
          message: "login confirmed",
          status: "success",
          data: sonuc[0],
        });
      } else {
        res.json({
          message: "Wrong username or password",
          status: "fail",
        });
      }
    } else {
      res.json({
        message: "Wrong username or password",
        status: "fail",
      });
    }
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred during the database operation" });
  }
});
app.post("/check", async (req, res) => {
  const requestData = req.body;
  try {
    const sorgudata = {
      username: requestData.username,
    };
    const sonuc = await InsertData("users", sorgudata, "sorgu");
    if (sonuc.length > 0) {
      if (sonuc[0].id === requestData.id) {
        res.json({
          message: "login confirmed",
          status: "success",
          data: sonuc[0],
        });
      } else {
        res.json({
          message: "Not user",
          status: "fail",
        });
      }
    } else {
      res.json({
        message: "Not user",
        status: "fail",
      });
    }
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred during the database operation" });
  }
});

app.post("/news", async (req, res) => {
  const requestData = req.body;
  try {
    const sonuc = await NewsGet(requestData.keyword);
    res.json({
      data: sonuc,
      status: "success",
    });
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred during the database operation" });
  }
});

const port = 8080;

app.listen(port, () => {
  console.log(`Sunucu ${port} portunda çalışıyor.`);
});

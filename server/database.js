const { initializeApp, cert } = require("firebase-admin/app");
const { getFirestore } = require("firebase-admin/firestore");

const serviceAccount = require("./newsfeed-c121e-7025eb774219.json");
initializeApp({
  credential: cert(serviceAccount),
});

const db = getFirestore();

async function InsertData(databasename, veri, verieklesorgu) {
  if (verieklesorgu === "sorgu") {
    const cityRef = db.collection(databasename);
    let sonuc;
    await cityRef
      .where("username", "==", veri.username)
      .get()
      .then((querySnapshot) => {
        if (querySnapshot.empty) {
          sonuc = [];
        } else {
          querySnapshot.forEach((doc) => {
            sonuc = [{ ...doc.data(), id: doc.id }];
          });
        }
      })
      .catch((error) => {
        console.error("Hata oluştu: ", error);
      });
    return sonuc;
  } else if (verieklesorgu === "ekle") {
    const docRef = db.collection(databasename);
    let sonuc;
    await docRef
      .add({ ...veri })
      .then(() => {
        sonuc = true;
      })
      .catch(() => {
        sonuc = false;
      });
    return sonuc;
  }
}

module.exports = InsertData;

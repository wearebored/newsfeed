const NewsAPI = require("newsapi");
const apiKey = process.env.NEWS_APP_KEY;
const newsapi = new NewsAPI(apiKey);

async function NewsGet(keyword) {
  let data;
  const params = { q: keyword, language: "en" };
  await newsapi.v2
    .everything(params)
    .then((response) => {
      data = response;
    })
    .catch(() => {
      data = false;
    });
  return data;
}

module.exports = NewsGet;

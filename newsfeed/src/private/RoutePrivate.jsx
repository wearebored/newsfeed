import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
function RoutePrivate() {
  const singin = useSelector((store) => store.signin.signin);

  return singin ? <Outlet /> : <Navigate to="/login" />;
}

export default RoutePrivate;

import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

function RouteNanPrivate() {
  const singin = useSelector((store) => store.signin.signin);

  return !singin ? <Outlet /> : <Navigate to="/news" />;
}
export default RouteNanPrivate;

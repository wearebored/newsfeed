import {
  setWarningModal,
  setWarningModalCloseAsync,
} from "../app/Slice/features/ModalSlice";

export function ModalErrorSuccess(dispatch, message, hatacode) {
  dispatch(
    setWarningModal({
      modal: true,
      modaldata: message,
      warning: hatacode,
    })
  );
  dispatch(setWarningModalCloseAsync());
}

import { ModalErrorSuccess } from "./Modalfunction";


export async function NewsData(url, data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const responseData = await response.json();
    if (responseData.status === "success") {
      return responseData.data.articles;
    } else {
      return false;
    }
  } catch (error) {
    return true;
  }
}
export async function LoginCheck(url, data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const responseData = await response.json();
    if (responseData.status === "success") {
      return responseData;
    } else {
      return false;
    }
  } catch (error) {
    return true;
  }
}
export async function LoginData(url, data, dispatch) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const responseData = await response.json();
    if (responseData.status === "success") {
      return responseData;
    } else {
      ModalErrorSuccess(dispatch, responseData.message, "error");
    }
  } catch (error) {
    console.error("Hata:", error);
  }
}
export async function RegisterData(url, data, navigate, dispatch) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const responseData = await response.json();
    if (responseData.status === "success") {
      navigate("/login");
      ModalErrorSuccess(dispatch, responseData.message, "success");
    } else {
      ModalErrorSuccess(dispatch, responseData.message, "error");
    }
  } catch (error) {
    console.error("Hata:", error);
  }
}

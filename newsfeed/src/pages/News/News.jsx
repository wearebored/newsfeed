import React, { useEffect, useState } from "react";
import { NewsContainer, NewsList } from "./News-styled";
import Navbar from "../../components/Navbar/Navbar";
import FilterComp from "../../components/FilterComp/FilterComp";
import { NewsData } from "../../helpers/postData";

function News() {
  const [data, setData] = useState("");
  const [onOf, setOnOf] = useState("");
  const [author, setAuthor] = useState("");
  const [time, setTime] = useState("");
  const [source, setSource] = useState("");
  const [totallist, setTotallist] = useState("");
  const [keyword, setKeyword] = useState("");

  useEffect(() => {
    const key = localStorage.getItem("keyword");
    const sour = localStorage.getItem("source");
    const tim = localStorage.getItem("time");
    const aut = localStorage.getItem("author");
    async function newDat() {
      let veri = await NewsData("http://localhost:8080/news", { keyword: key });
      setData(veri);
    }
    if (key && sour && tim && aut) {
      newDat();
    }
    setAuthor(aut);
    setTime(tim);
    setSource(sour);
  }, []);
  useEffect(() => {
    if (time && source && author && keyword) {
      localStorage.setItem("keyword", keyword);
      localStorage.setItem("source", source);
      localStorage.setItem("time", time);
      localStorage.setItem("author", author);
    }
  }, [time, source, author, keyword]);

  useEffect(() => {
    if (data) {
      const authordata = [];
      const timedata = [];
      const sourcedata = [];
      data.forEach((e) => {
        !authordata.includes(e.author) && authordata.push(e.author);
        !timedata.includes(e.publishedAt) && timedata.push(e.publishedAt);
        !sourcedata.includes(e.source.name) && sourcedata.push(e.source.name);
      });
      setAuthor(authordata);
      setTime(timedata);
      setSource(sourcedata);
      setTotallist({
        author: authordata,
        time: timedata,
        source: sourcedata,
      });
    }
  }, [data]);

  return (
    <NewsContainer>
      <Navbar setData={setData} keyword={keyword} setKeyword={setKeyword} />
      <NewsList>
        {data && (
          <div className="filterlist">
            <p>Filter</p>
            {onOf !== "" ? (
              <div className="filterselect">
                <FilterComp
                  setOnOf={setOnOf}
                  onOf={onOf}
                  totallist={totallist}
                  liste={
                    onOf === "author"
                      ? author
                      : onOf === "time"
                      ? time
                      : onOf === "source" && source
                  }
                  setListe={
                    onOf === "author"
                      ? setAuthor
                      : onOf === "time"
                      ? setTime
                      : onOf === "source" && setSource
                  }
                />
              </div>
            ) : (
              <div>
                <button onClick={() => setOnOf("author")}>Select Author</button>
                <button
                  style={{ margin: "0 40px" }}
                  onClick={() => setOnOf("time")}
                >
                  Select time
                </button>
                <button onClick={() => setOnOf("source")}>select source</button>
              </div>
            )}
          </div>
        )}
        {data &&
          data.map((e, i) => {
            if (
              author.includes(e.author) &&
              time.includes(e.publishedAt) &&
              source.includes(e.source.name)
            ) {
              return (
                <div className="habercard" key={i}>
                  <div className="image">
                    <img src={e.urlToImage} alt="" />
                  </div>
                  <div>
                    {" "}
                    <h1>{e.title}</h1>
                    <p>{e.author}</p>
                    <p>{e.publishedAt}</p>
                    <p>{e.description}</p>
                    <p>{e.content}</p>
                    <a href={e.url}>Details</a>
                  </div>
                </div>
              );
            } else {
              return null;
            }
          })}
      </NewsList>
    </NewsContainer>
  );
}

export default News;

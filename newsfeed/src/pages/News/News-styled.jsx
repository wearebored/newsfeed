import styled from "styled-components";

export const NewsContainer = styled.div``;
export const NewsList = styled.div`
  padding-top: 46px;
  .filterlist {
    width: 100%;
    min-height: 100px;
    background-color: red;
  }
  .habercard {
    margin: 30px;
    display: flex;
    .image {
      img {
        max-width: 600px;
      }
    }
  }
`;

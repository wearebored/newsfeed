import React, { useState } from "react";
import { RegisterContainer } from "./Register-styled";
import { useNavigate } from "react-router-dom";
import { RegisterData } from "../../helpers/postData";
import { useDispatch } from "react-redux";

function Register() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  function RegisterClick() {
    if (firstname && lastname && username && password) {
      const data = {
        firstname,
        lastname,
        username,
        password,
      };
      RegisterData("http://localhost:8080/register", data, navigate, dispatch);
    }
  }
  return (
    <RegisterContainer>
      <h3>Register</h3>
      <div className="registerform">
        <label htmlFor="firstname">Firstname</label>
        <input
          onChange={(e) => setFirstname(e.target.value)}
          value={firstname}
          type="text"
          name="firstname"
          id="firstname"
        />
        <label htmlFor="lastname">Lastname</label>
        <input
          onChange={(e) => setLastname(e.target.value)}
          value={lastname}
          type="text"
          name="lastname"
          id="lastname"
        />
        <label htmlFor="username">Username</label>
        <input
          onChange={(e) => setUsername(e.target.value)}
          value={username}
          type="text"
          name="username"
          id="username"
        />
        <label htmlFor="password">Password</label>
        <input
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          type="password"
          name="password"
          id="password"
        />
        <button onClick={RegisterClick}>Register</button>
        <p className="loginbuton">
          I have an account{" "}
          <span onClick={() => navigate("/login")}>Log In</span>
        </p>
      </div>
    </RegisterContainer>
  );
}

export default Register;

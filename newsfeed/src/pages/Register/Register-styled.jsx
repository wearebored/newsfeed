import styled from "styled-components";

export const RegisterContainer = styled.div`
  background: #9ed4fa;
  width: 100vw;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  .loginbuton {
    span {
      color: blue;
      text-decoration: underline;
      &:hover{
        cursor: pointer;
      }
    }
  }
  h3 {
    font-size: 40px;
  }
  input {
    width: 300px;
    height: 30px;
    border-radius: 10px;
    padding-left: 10px;
  }
  label {
    padding-left: 10px;
  }
  .registerform {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 5px;
    button {
      margin-top: 20px;
      width: 316px;
      height: 30px;
      border-radius: 10px;
    }
  }
`;

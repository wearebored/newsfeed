import { Navigate } from "react-router-dom";

function RouterPage() {
  return <Navigate to={"/login"} />;
}

export default RouterPage;

import React, { useState } from "react";
import { RegisterContainer } from "../Register/Register-styled";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { LoginData } from "../../helpers/postData";
import { setSignIn } from "../../app/Slice/features/SignInSlice";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  async function LoginClick() {
    if (username && password) {
      const data = {
        username,
        password,
      };
      const login = await LoginData(
        "http://localhost:8080/login",
        data,
        dispatch
      );

      if (login) {
        dispatch(
          setSignIn({
            signin: true,
            username: login.data.username,
            firstname: login.data.firstname,
            lastname: login.data.lastname,
            id: login.data.id,
          })
        );
        localStorage.setItem("id", login.data.id);
        localStorage.setItem("username", login.data.username);
      }
    }
  }
  return (
    <RegisterContainer>
      <h3>Login</h3>
      <div className="registerform">
        <label htmlFor="username">Username</label>
        <input
          onChange={(e) => setUsername(e.target.value)}
          value={username}
          type="text"
          name="username"
          id="username"
        />
        <label htmlFor="password">Password</label>
        <input
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          type="password"
          name="password"
          id="password"
        />
        <button onClick={LoginClick}>Login</button>
        <p className="loginbuton">
          {"I don't have an account "}
          <span onClick={() => navigate("/register")}>Register</span>
        </p>
      </div>
    </RegisterContainer>
  );
}

export default Login;

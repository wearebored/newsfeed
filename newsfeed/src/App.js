import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import RoutePrivate from "./private/RoutePrivate";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import WarningModal from "./components/WarningModal/WarningModal";
import RouteNanPrivate from "./private/RouteNanPrivate";
import News from "./pages/News/News";
import RouterPage from "./pages/RouterPage/RouterPage";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { LoginCheck } from "./helpers/postData";
import { setSignIn } from "./app/Slice/features/SignInSlice";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    async function check() {
      const id = localStorage.getItem("id");
      const username = localStorage.getItem("username");
      if (id && username) {
        dispatch(
          setSignIn({
            signin: true,
            username: "",
            firstname: "",
            lastname: "",
            id: "",
          })
        );
        const data = {
          username,
          id,
        };

        const checkdata = await LoginCheck("http://localhost:8080/check", data);
        if (checkdata) {
          dispatch(
            setSignIn({
              signin: true,
              username: checkdata.data.username,
              firstname: checkdata.data.firstname,
              lastname: checkdata.data.lastname,
              id: checkdata.data.id,
            })
          );
        } else {
          dispatch(
            setSignIn({
              signin: false,
              username: "",
              firstname: "",
              lastname: "",
              id: "",
            })
          );
          localStorage.removeItem("id");
          localStorage.removeItem("username");
        }
      }
    }
    check();
  }, [dispatch]);

  return (
    <div className="App">
      <WarningModal />
      <BrowserRouter>
        <Routes>
          <Route element={<RoutePrivate />}>
            <Route path="/news" element={<News />} />
          </Route>
          <Route element={<RouteNanPrivate />}>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Route>
          <Route path="/" element={<RouterPage />} />
          <Route path="*" element={<RouterPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

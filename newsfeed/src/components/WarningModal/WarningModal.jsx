import React from "react";
import {
  WarningModalContainer,
  WarningModalTextCon,
} from "./WarningModal-styled";
import { useSelector } from "react-redux";

function WarningModal() {
  const { modal, modaldata, warning } = useSelector((store) => store.modal);
  if (modal) {
    return (
      <WarningModalContainer
        style={{ backgroundColor: warning === "error" && "red" }}
      >
        <WarningModalTextCon>
          {modaldata === "" ? <p>Your process is complete</p> : modaldata}
        </WarningModalTextCon>
      </WarningModalContainer>
    );
  } else {
    return <div></div>;
  }
}

export default WarningModal;

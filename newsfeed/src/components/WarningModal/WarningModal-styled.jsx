import { styled } from "styled-components";

export const WarningModalContainer = styled.div`
  height: 70px;
  width: 100%;
  flex-shrink: 0;
  background: #4eff32;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 100;
`;
export const WarningModalTextCon = styled.div`
  color: white;
  font-family: Poppins;
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;

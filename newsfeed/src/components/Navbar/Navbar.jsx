import React from "react";
import { NavbarContainer } from "./Nawbar-styled";
import { useDispatch, useSelector } from "react-redux";
import { setSignIn } from "../../app/Slice/features/SignInSlice";
import { NewsData } from "../../helpers/postData";

function Navbar({ setData, keyword, setKeyword }) {
  const dispatch = useDispatch();

  function exitClick() {
    localStorage.removeItem("id");
    localStorage.removeItem("username");

    dispatch(
      setSignIn({
        signin: false,
        username: "",
        firstname: "",
        lastname: "",
        id: "",
      })
    );
  }
  const firstname = useSelector((store) => store.signin.firstname);
  return (
    <NavbarContainer>
      <h3>Welcome {firstname}</h3>

      <div className="filterdiv">
        <label htmlFor="">Search news title</label>
        <input
          style={{ margin: "0 10px" }}
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
          type="text"
        />
        <button
          onClick={async () => {
            setData(await NewsData("http://localhost:8080/news", { keyword }));
          }}
          style={{ width: "70px" }}
        >
          Search
        </button>
      </div>
      <button onClick={exitClick} className="exitbuton">
        Exit
      </button>
    </NavbarContainer>
  );
}

export default Navbar;

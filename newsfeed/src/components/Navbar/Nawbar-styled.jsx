import styled from "styled-components";

export const NavbarContainer = styled.div`
  background: gray;
  width: 100%;
  min-height: 50px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  position: absolute;

  .filterdiv {
    position: relative;

  }
  .exitbuton {
    width: 60px;
    border-radius: 5px;
    border: solid 0px;
    &:hover {
      cursor: pointer;
    }
  }
`;

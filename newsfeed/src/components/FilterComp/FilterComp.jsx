import React from "react";

function FilterComp({ setOnOf, onOf, liste, setListe, totallist }) {
  if (onOf === "author") {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
        }}
      >
        <button onClick={() => setOnOf("")}>Geri</button>
        {totallist?.author.map((e, i) => (
          <div key={i}>
            <input
              onChange={() => {
                liste.includes(e)
                  ? setListe(liste.filter((item) => item !== e))
                  : setListe([...liste, e]);
              }}
              checked={liste.includes(e)}
              type="checkbox"
              name={e}
              id={e}
            />
            <label htmlFor={e}>{e}</label>
          </div>
        ))}
      </div>
    );
  } else if (onOf === "time") {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
        }}
      >
        <button onClick={() => setOnOf("")}>Geri</button>
        {totallist?.time.map((e, i) => (
          <div key={i}>
            <input
              onChange={() => {
                liste.includes(e)
                  ? setListe(liste.filter((item) => item !== e))
                  : setListe([...liste, e]);
              }}
              checked={liste.includes(e)}
              type="checkbox"
              name={e}
              id={e}
            />
            <label htmlFor={e}>{e}</label>
          </div>
        ))}
      </div>
    );
  } else if (onOf === "source") {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
        }}
      >
        <button onClick={() => setOnOf("")}>Geri</button>
        {totallist?.source.map((e, i) => (
          <div key={i}>
            <input
              onChange={() => {
                liste.includes(e)
                  ? setListe(liste.filter((item) => item !== e))
                  : setListe([...liste, e]);
              }}
              checked={liste.includes(e)}
              type="checkbox"
              name={e}
              id={e}
            />
            <label htmlFor={e}>{e}</label>
          </div>
        ))}
      </div>
    );
  }
}

export default FilterComp;

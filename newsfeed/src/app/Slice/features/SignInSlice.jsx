import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  signin: false,
  username: "",
  firstname: "",
  lastname: "",
  id:"",
};

const SignInSlice = createSlice({
  name: "signin",
  initialState,
  reducers: {
    setSignIn: (s, a) => {
      s.signin = a.payload.signin;
      s.username = a.payload.username;
      s.firstname = a.payload.firstname;
      s.lastname = a.payload.lastname;
      s.id = a.payload.id;
    },
  },
});

export const { setSignIn } = SignInSlice.actions;

export default SignInSlice.reducer;

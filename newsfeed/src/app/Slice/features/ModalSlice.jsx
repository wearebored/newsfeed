import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modal: false,
  modaldata: "",
  warning: "",
};

const ModalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    setWarningModal: (s, a) => {
      s.modal = a.payload.modal;
      s.modaldata = a.payload.modaldata;
      s.warning = a.payload.warning;
    },
  },
});

export const setWarningModalCloseAsync = () => async (dispatch) => {
  setTimeout(() => {
    dispatch(setWarningModal({ modal: false, modaldata: "", warning: "" }));
  }, 4000);
};

export const { setWarningModal } = ModalSlice.actions;

export default ModalSlice.reducer;

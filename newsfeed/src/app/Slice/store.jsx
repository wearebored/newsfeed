import { configureStore } from "@reduxjs/toolkit";
import ModalSlice from "./features/ModalSlice";
import SignInSlice from "./features/SignInSlice";

export const store = configureStore({
  reducer: {
    modal: ModalSlice,
    signin: SignInSlice,
  },
});
